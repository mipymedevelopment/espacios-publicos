import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.scss']
})

export class ArtistsComponent implements OnInit {

  constructor(private api:ApiService, private router:Router) { }

  destacados:any = [{image:'',desc:'loading...'}]
  index = 0
  animate = true;
  timeout:any;

  ngOnInit(): void {
    this.api.getFeaturedArtists().subscribe(data=>{
      this.destacados = data
    })
    setTimeout(() => {
      this.animate=false
    }, 2000);

    this.timeout = setTimeout(()=>{
      this.changeIndex( (this.index+1)%4 )
    },6000)

  }

  changeIndex(i:number){
    clearTimeout(this.timeout)
    this.index = i;
    this.animate=true
    setTimeout(()=>{
      this.animate=false;
    },1000)
    
    this.timeout = setTimeout(()=>{
      this.changeIndex( (this.index+1)%4 )
    },6000)
  }

  handleRedirect(){
    this.router.navigate(['/artist',this.destacados[this.index]._id])
  }
}
