import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  constructor(private api:ApiService) { }

  news:any = [{image:''},{image:''},{image:''},{image:''}]

  ngOnInit(): void {
    this.api.getNews(4).subscribe(response =>{
      this.news = response
    })
  }
}
