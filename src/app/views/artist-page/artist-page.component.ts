import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss']
})
export class ArtistPageComponent implements OnInit {

  constructor(private api: ApiService, private activedRoute: ActivatedRoute) { }

  artist:any = {image:'', name:'', desc:'Loading...'};
  works:any = []

  ngOnInit(): void {
    this.activedRoute.paramMap.subscribe(params =>{
      let id = params.get('id')
      this.api.getArtistInfo(id).subscribe(response =>{
        this.artist = response[0]
      })
      this.api.getWorks(id).subscribe(response =>{
        this.works = response
        console.log(this.works)
      })
    })
  }

}
