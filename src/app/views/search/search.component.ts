import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import levenshtein from 'src/app/utils/levenshtein'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(private api:ApiService) { }

  artists:any = []
  matched:Array<any> = []
  match:boolean = false;

  busqueda = ""

  ngOnInit(): void {
    this.api.getAllArtists().subscribe(response =>{
      this.artists = response
      console.log(this.artists)
    })
  }

  handleChange(){
        
    let new_matched = []
    for(let i=0 ; i<this.artists.length ; i++){
      let palabras = this.artists[i].name.split(' ')
      for(let j=0 ; j<palabras.length ; j++){
        if(levenshtein(this.busqueda, palabras[j]) <= 2 && palabras[j].length > 3){
          new_matched.push(this.artists[i])
        }        
      }
    }
    this.matched = new_matched;
  }
 
}
