import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ArtistPageComponent } from './views/artist-page/artist-page.component';
import { SearchComponent } from './views/search/search.component';

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'search', component: SearchComponent},
  {path: ':view', component: MainComponent},
  {path: 'artist/:id', component: ArtistPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
