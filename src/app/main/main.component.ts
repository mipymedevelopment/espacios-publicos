import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private activeRoute:ActivatedRoute, private router:Router) { }


  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe(params=>{
      let element = params.get('view')

      switch(element){
        case null:
          document.getElementById('home').scrollIntoView({behavior:'smooth',block:'end'})
          break
        case 'art':
          document.getElementById('featured-artists').scrollIntoView({behavior:'smooth',block:'end'})
          break
        case 'news':
          document.getElementById('news').scrollIntoView({behavior:'smooth',block:'end'})
          break
        default:
          break
      }
    })
  }

}
