import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-news-article',
  templateUrl: './news-article.component.html',
  styleUrls: ['./news-article.component.scss']
})
export class NewsArticleComponent implements OnInit {

  constructor() { }

  @Input() imgsrc:string;
  @Input() title:string;
  @Input() resume:string;

  show:boolean = false;

  ngOnInit(): void {
  }


  hover(){
    this.show = true;
  }
  leaves(){
    this.show = false;
  }
}
