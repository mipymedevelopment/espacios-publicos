import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { MainComponent } from './main/main.component';
import { HomeComponent } from './views/home/home.component';
import { ArtistsComponent } from './views/artists/artists.component';
import { ApiService } from './services/api.service';
import { ArtistPageComponent } from './views/artist-page/artist-page.component';
import { NewsComponent } from './views/news/news.component';
import { NewsArticleComponent } from './components/news-article/news-article.component';
import { SearchComponent } from './views/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    ArtistsComponent,
    ArtistPageComponent,
    NewsComponent,
    NewsArticleComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
