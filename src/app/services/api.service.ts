import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  apiUrl:string = 'http://localhost:4001';

  getFeaturedArtists(){
    return this.http.get(`${this.apiUrl}/artists/destacados`)
  }

  getAllArtists(){
    return this.http.get(`${this.apiUrl}/artists?id=all`)
  }

  getArtistInfo(id:string){
    return this.http.get(`${this.apiUrl}/artists?id=${id}`)
  }

  getWorks(id:string){
    return this.http.get(`${this.apiUrl}/works?id=${id}`)
  }

  getNews(qty:number){
    return this.http.get(`${this.apiUrl}/news?qty=${qty}`)
  }


}
